import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="qualidade", # Replace with your own username
    version="0.0.2",
    author="Ademar Arvati Filho",
    author_email="arvati@hotmail.com",
    description="A package to use to practice some quality control and assurance tools",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vanaware/qualidade",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'numpy',
        'pandas',
        'statistics',
        'matplotlib',
        'scipy',
        'sympy',
        'scikit-learn'
    ],
)
